#pragma once

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "Obilisk/Log.h"

#ifdef OB_PLATFORM_WINDOWS
	#include "Windows.h"
#endif

