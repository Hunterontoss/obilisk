#include "obkpch.h"
#include "Application.h"
#include "Obilisk/Events/ApplicationEvent.h"
#include <GLFW/glfw3.h>

namespace Obilisk
{

#define BIND_EVENT_FN(x) std::bind(&Application::x, this, std::placeholders::_1)

	Obilisk::Application::Application()
	{
		m_Window = std::unique_ptr<Window>(Window::Create());
		m_Window->SetEventCallback(BIND_EVENT_FN(OnEvent));
	}

	Obilisk::Application::~Application()
	{
	}

	void Obilisk::Application::OnEvent(Event& e)
	{
		OB_CORE_INFO("{0}", e);
	}

	void Application::Run()
	{
		
		while (m_Running)
		{
			glClearColor(0.5, 0.5, 0.5, 1);
			glClear(GL_COLOR_BUFFER_BIT);
			m_Window->OnUpdate();
		}
	}
}