#pragma once

#include "obkpch.h"

#include "Obilisk/Core.h"
#include "Obilisk/Events/Event.h"

namespace Obilisk
{
	struct WindowProps
	{
		std::string Title;
		unsigned int Width;
		unsigned int Height;

		WindowProps(const std::string& title = "Obilisk_Engine",
					unsigned int width = 1440,
					unsigned int height = 900
			) : Title(title), Width(width), Height(height)
		{

		}
	};

	// Interface representing a desktop system based Window

	class OBILISK_API Window
	{
	public:
		using EventCallbackFn = std::function<void(Event&)>;

		virtual ~Window() {}

		virtual void OnUpdate() = 0;

		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;

		// Window  attributes
		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void SetVSync(bool enable) = 0;
		virtual bool IsVSync() const = 0;

		static Window* Create(const WindowProps& props = WindowProps());
	};

}
