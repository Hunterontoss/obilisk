#pragma once

#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace Obilisk
{
	class OBILISK_API Log
	{
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};
}

#define OB_CORE_TRACE(...) ::Obilisk::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define OB_CORE_INFO(...) ::Obilisk::Log::GetCoreLogger()->info(__VA_ARGS__)
#define OB_CORE_WARNING(...) ::Obilisk::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define OB_CORE_ERROR(...) ::Obilisk::Log::GetCoreLogger()->error(__VA_ARGS__)
#define OB_CORE_FATAL(...) ::Obilisk::Log::GetCoreLogger()->fatal(__VA_ARGS__)

#define OB_TRACE(...) ::Obilisk::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define OB_INFO(...) ::Obilisk::Log::GetCoreLogger()->info(__VA_ARGS__)
#define OB_WARNING(...) ::Obilisk::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define OB_ERROR(...) ::Obilisk::Log::GetCoreLogger()->error(__VA_ARGS__)
#define OB_FATAL(...) ::Obilisk::Log::GetCoreLogger()->fatal(__VA_ARGS__)
