#pragma once

#ifdef OB_PLATFORM_WINDOWS

extern Obilisk::Application* Obilisk::CreateApplication();

int main(int argc, char** argv)
{
	Obilisk::Log::Init();
	OB_CORE_WARNING("Initial Log");
	int a = 1;
	OB_INFO("Hi There! {0}", a);

	auto app = Obilisk::CreateApplication();
	app->Run();
	delete app;
}

#endif // OB_PLATFORM_WINDOWS
