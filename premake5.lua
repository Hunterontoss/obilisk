workspace "Obilisk"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Distribution"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GLFW"] = "Obilisk/vendor/GLFW/include"

include "Obilisk/vendor/GLFW"

project "Obilisk"
	location "Obilisk"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%prj.name")
	objdir ("binint/" .. outputdir .. "/%prj.name")

	pchheader "obkpch.h"
	pchsource "%{prj.name}/src/obkpch.cpp"
	
	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.GLFW}"
	}

	links
	{
		"GLFW",
		"opengl32.lib"
	}

	filter "system:windows"
		cppdialect "C++20"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"OB_PLATFORM_WINDOWS",
			"OB_BUILD_DLL"
		}

		postbuildcommands
		{
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter "configurations:Debug"
		defines "OB_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "OB_RELEASE"
		symbols "On"

	filter "configurations:Distribution"
		defines "OB_DISTRIBUTION"
		symbols "On"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%prj.name")
	objdir ("binint/" .. outputdir .. "/%prj.name")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"Obilisk/vendor/spdlog/include",
		"Obilisk/src"
	}

	links 
	{
		"Obilisk"
	}

	filter "system:windows"
		cppdialect "C++20"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"OB_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "OB_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "OB_RELEASE"
		symbols "On"

	filter "configurations:Distribution"
		defines "OB_DISTRIBUTION"
		symbols "On"